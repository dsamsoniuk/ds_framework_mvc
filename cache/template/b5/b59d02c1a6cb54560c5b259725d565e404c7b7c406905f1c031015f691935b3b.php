<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Main/index.html */
class __TwigTemplate_4181ff648d10e6c4a82fc2d68ef580c37e6f3242c21150c274cef20d29d95585 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("index.html", "Main/index.html", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <h4>SubTemplate</h4>
    <h3>Hello</h3>
    <button class=\"btn btn-primary\">jakisKlik</button>
        </div>
    </div>
    <div class=\"row\">
";
        // line 19
        echo "
    </div>
    <div class=\"row\">
        <div class=\"col-sm-12\">
            ";
        // line 23
        if (($context["users"] ?? null)) {
            // line 24
            echo "            <h4>Przyklad danych z db</h4>
            <ul>
                ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 27
                echo "                <li>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "id", [], "any", false, false, false, 27), "html", null, true);
                echo " - ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "email", [], "any", false, false, false, 27), "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "            </ul>
            ";
        }
        // line 31
        echo "        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <form action=\"\" method=\"post\">
                <input type=\"text\" name=\"test[name]\" id=\"\">
                <input type=\"submit\" value=\"wyslij\">
            </form>
        </div>
    
    </div>

</div>





";
    }

    public function getTemplateName()
    {
        return "Main/index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 31,  88 => 29,  77 => 27,  73 => 26,  69 => 24,  67 => 23,  61 => 19,  50 => 6,  46 => 5,  35 => 3,);
    }

    public function getSourceContext()
    {
        return new Source("", "Main/index.html", "/var/www/html/mvc/src/Template/Main/index.html");
    }
}
