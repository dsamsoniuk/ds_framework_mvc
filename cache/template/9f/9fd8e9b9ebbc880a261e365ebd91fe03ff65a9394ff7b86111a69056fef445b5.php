<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html */
class __TwigTemplate_2e2af6eb0a18084b494b573e0d1ed15dd3db6271bb5017d2df6b12ecb0b86f85 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'style' => [$this, 'block_style'],
            'body' => [$this, 'block_body'],
            'javascript' => [$this, 'block_javascript'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<html>
    <head></head>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/bootstrap4/bootstrap.min.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/style.css\">
    ";
        // line 5
        $this->displayBlock('style', $context, $blocks);
        // line 7
        echo "    <body>
        <div class=\"container\">
    
                    <header class=\"blog-header py-3\">
                        <div class=\"row flex-nowrap justify-content-between align-items-center\">
                          <div class=\"col-4 pt-1\">
                            <a class=\"text-muted\" href=\"#\">Subscribe</a>
                          </div>
                          <div class=\"col-4 text-center\">
                            <h3>
                                <a class=\"blog-header-logo text-dark\" href=\"#\">Large</a>
                            </h3>
                          </div>
                          <div class=\"col-4 d-flex justify-content-end align-items-center\">
                            <a class=\"text-muted\" href=\"#\">
                              <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"mx-3\"><circle cx=\"10.5\" cy=\"10.5\" r=\"7.5\"></circle><line x1=\"21\" y1=\"21\" x2=\"15.8\" y2=\"15.8\"></line></svg>
                            </a>
                            
                            <a class=\"btn btn-sm btn-outline-secondary\" href=\"https://getbootstrap.com/docs/4.0/examples/blog/\">Przyklad Szablonu</a>

                            <a class=\"btn btn-sm btn-outline-secondary\" href=\"#\">Sign up</a>
                          </div>
                        </div>
                      </header>
           
                      <div class=\"nav-scroller py-1 mb-2\">
                        <nav class=\"nav d-flex justify-content-between\">
                          <a class=\"p-2 text-muted\" href=\"/\">index</a>
                          <a class=\"p-2 text-muted\" href=\"/kontakt/123\">Kontakt</a>
                          <a class=\"p-2 text-muted\" href=\"#\">Technology</a>
                          <a class=\"p-2 text-muted\" href=\"#\">Design</a>
                          <a class=\"p-2 text-muted\" href=\"#\">Design</a>
                          <a class=\"p-2 text-muted\" href=\"#\">Design</a>
                          <a class=\"p-2 text-muted\" href=\"#\">Design</a>
                          <a class=\"p-2 text-muted\" href=\"#\">Design</a>
                        </nav>
                      </div>

                      <div class=\"jumbotron p-3 p-md-5 text-white rounded bg-dark\">
                        <div class=\"col-md-6 px-0\">
                          <h1 class=\"display-4 font-italic\">Title of a longer featured blog post</h1>
                          <p class=\"lead my-3\">Multiple lines of text that form the lede, informing new readers quickly and efficiently about what's most interesting in this post's contents.</p>
                          <p class=\"lead mb-0\"><a href=\"#\" class=\"text-white font-weight-bold\">Continue reading...</a></p>
                        </div>
                      </div>
                      ";
        // line 53
        echo "                      
                      <div class=\"row\">
                        <div class=\"col-sm-10\">
                          ";
        // line 56
        $this->loadTemplate("alerts.html", "index.html", 56)->display($context);
        echo " <br>

                          ";
        // line 58
        $this->displayBlock('body', $context, $blocks);
        // line 59
        echo "                        </div>
                        <div class=\"col-sm-2\">";
        // line 60
        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["global"] ?? null), "widget", [], "any", false, false, false, 60), "index", [], "any", false, false, false, 60);
        echo "</div>
                      </div>
        </div>
  
        <script src=\"/js/main.js\"></script>
        <script src=\"/js/package/jquery/jquery-3.4.1.slim.min.js\"></script>
        <script src=\"/js/package/jquery/popper.min.js\"></script>
        <script src=\"/js/package/bootstrap4/bootstrap.min.js\"></script>
        ";
        // line 68
        $this->displayBlock('javascript', $context, $blocks);
        // line 70
        echo "    </body>
</html>";
    }

    // line 5
    public function block_style($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
    }

    // line 58
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 68
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 69
        echo "        ";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 69,  142 => 68,  136 => 58,  132 => 6,  128 => 5,  123 => 70,  121 => 68,  110 => 60,  107 => 59,  105 => 58,  100 => 56,  95 => 53,  48 => 7,  46 => 5,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "index.html", "/var/www/html/mvc/src/Template/index.html");
    }
}
