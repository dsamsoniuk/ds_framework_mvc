# Micro framework MVC DS

* Contains: composer, routing, twig, LessQL, Controller, Manager

### How create page:
* Add controller in src/Controller
* Add track in class Core/Routing.php
* Add template in src/Template directory

Optional:
* Add manager, this is kind of helper there you can storage all necessary function like in example calculate numbers etc.

### Connecting to Database:
* Use ./config.ini where are all informations about host, login, password etc.

### LessQL - ORM - usage:
* https://github.com/morris/lessql/blob/master/doc/api.md

### Commands:
* Refresh class map if was added new:

composer dump-autoload -o

### Configuration for server
* NGINX - nginx.config
* Apache - apache.config

### Example controller:
@
    // Example
    
    $post = $this->session; // session, cookie, get, post, server
    $this->session->setAlert('danger', 'Ojojojoj');
    d(123); // var_dump
    
    $this->session->setAlert('primary', 'Ojojojoj');
    //Example manager
    $this->session->set('bb','232323');
    
    $manager        = new MainManager();
    $randomNumer    = $manager->randomNumber();
    //Example:  https://github.com/morris/lessql/blob/master/doc/api.md
    $db             = new DB();
    $users          = $db->getConnect()->user()->fetchAll();
@


### TODO:
*  sa problemy z zapisem cache uprawnienia sa niedostosowane i nie maja write access
