<?php

namespace Controller;

use Core\Container;
use Manager\MainManager;
use Core\DB;
use DateTime;

class WidgetController extends Container {

    public function index(){
        $post = $this->post; // session, cookie, get, post, server
        // $this->session->setAlert('danger', 'Ojojojoj');
        // d($this->session->get('alerts'));
        // d($request['session']);
        // Example manager
        $manager        = new MainManager();
        $randomNumer    = $manager->randomNumber();

        //Example:  https://github.com/morris/lessql/blob/master/doc/api.md
        // $db             = new DB();
        // $users          = $db->getConnect()->user()->fetchAll();
     

        return $this->renderTemplate("Widget/widget.html", [
            'currentDate' => new DateTime(),
            'randNum' => $randomNumer
            ]);
    }
}