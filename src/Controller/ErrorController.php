<?php

namespace Controller;

use Core\Container;

class ErrorController extends Container {

    public function error404(){

        return $this->renderTemplate("error404.html");
    }
}