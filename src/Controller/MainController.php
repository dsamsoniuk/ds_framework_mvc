<?php

namespace Controller;

use Core\Container;
use Manager\MainManager;
use Core\DB;

class MainController extends Container {
    /**
     * @return html
     */
    public function index(){
        // $post = $this->session; // session, cookie, get, post, server
        // $this->session->setAlert('danger', 'Ojojojoj');
        // $this->session->setAlert('primary', 'Ojojojoj');
        // d($request['session']);
        // Example manager
        // $this->session->set('bb','232323');

        $manager        = new MainManager();
        $randomNumer    = $manager->randomNumber();

        //Example:  https://github.com/morris/lessql/blob/master/doc/api.md
        $db             = new DB();
        $users          = $db->getConnect()->user()->fetchAll();
     

        return $this->renderTemplate("Main/index.html", ['users' => $users]);
    }
    /** 
     * @return JsonResponse
     */
    public function getRandomNumber(){
        $manager        = new MainManager();
        $randomNumer    = $manager->randomNumber();

        return $this->renderJson([
            'randomNumber' => $randomNumer,
        ]);
    }
}