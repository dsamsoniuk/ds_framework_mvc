<?php

namespace Controller;

use Core\Container;

class KontaktController extends Container {

    public function index(){

        return $this->renderTemplate("Kontakt/index.html", [
            'id' => $this->get->get('id')
        ]);
    }
}