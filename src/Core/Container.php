<?php

namespace Core;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Request;

class Container {

    public $post;
    public $get;
    public $server;
    public $cookie;
    public $session;
    public $globalData;

    public function __construct(){
        $this->get      = new Request\Get();
        $this->post     = new Request\Post();
        $this->cookie   = new Request\Cookie();
        $this->server   = new Request\Server();
        $this->session  = new Request\Session();
        $this->globalData = [];
    }

    public function addGlobalData($data = []){
        $this->globalData = array_merge($this->globalData, $data);
    }

    public function setDataGET($data){
        $this->get->setList($data);
    }
    
    /**
     * Render template
     */
    public function renderTemplate($path = '', $data = []){
        $twig   = new Environment( new FilesystemLoader("../src/Template"), array( 
            "cache"         => "../cache/template",
            'auto_reload'   => true,
        ));
        $tpl    = $twig->loadTemplate( $path );
        $data   = array_merge($data, $this->globalData);
        return $tpl->render( $data );
    }
    /** 
     * Response JSON 
     * */
    public function renderJson($data = []){
        header('Content-Type: application/json');
        return json_encode($data);
    }
}