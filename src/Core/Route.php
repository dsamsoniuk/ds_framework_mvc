<?php

namespace Core;
use Request;

class Route extends Routing {

    public $server;

    public function __construct(){
        $this->server = new Request\Server();
    }
    
    /** */
    public function getControllerByRoute(){
        $subject    = $this->server->get('REQUEST_URI');
        $exUrl      = explode('?', $subject);
        $subject    = $exUrl[0];
        $getData    = isset($exUrl[1]) ? $exUrl[1] : '';
        foreach ($this->tracks as $i => $track) {
            $replaceVars    = isset($track['vars']) ? $track['vars'] : [];
            $pattern        = $this->replaceTrackVars($track['path'], $replaceVars);
            if (preg_match($pattern, $subject)) {
                $unregDataGet   = $this->prepareGetDataUnregistered($getData);
                $regDataGet     = $this->prepareGetDataRegistered($subject, $track['path']);
                $track['GET']   = array_merge($unregDataGet, $regDataGet);
                return $track;
            }
        }
    }

    private function replaceTrackVars($track = '', $vars = []){
        foreach ($vars as $i => $v) {
            $track  = str_replace('{'.$i.'}', $this->typeVars[$v], $track);
        }
        $track  = str_replace('/', '\\/', $track);
        $track  = '/^'.$track.'$/';
        return $track;
    }

    private function prepareGetDataUnregistered($subUri = ''){
        $data = [];
        if ($subUri) {
            $vals = explode('&', $subUri);
            foreach ($vals as $v) {
                $d = explode('=', $v);
                $data[ (string) $d[0] ] = $d[1];
            }
        }

        return $data;
    }
    public function prepareGetDataRegistered($uri = '', $splitedUriPath = ''){
        $uriReg             = str_replace('{', '', $splitedUriPath);
        $uriReg             = str_replace('}', '', $uriReg);
        $splitedUri         = explode('/', $uri);
        $splitedUriPath     = explode('/', $uriReg);
        $data               = [];
        if ($splitedUri[0] != "") {
            throw new \Exception("Nie poprawny routing");
        }
        foreach ($splitedUri as $i => $v) {
            if ($splitedUri[$i] === $splitedUriPath[$i]) {
                continue;
            }
            $data[$splitedUriPath[$i]] =  $splitedUri[$i]; 
        }
        return $data;
    }

}