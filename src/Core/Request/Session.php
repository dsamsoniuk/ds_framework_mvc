<?php

namespace Request;

class Session implements Action {

    public function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    public function get($name)
    {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
    }

    public function getAll(){
        return $_SESSION;
    }

    public function setList($list)
    {
        $_SESSION = array_merge($list, $_SESSION);
    }

    public function remove($name)
    {
        unset($_SESSION[$name]);
    }

    /** $type = danger/success/warnign */
    public function setAlert($type = '', $text = ''){
        $alerts = $this->get('alerts') ?: [];
        $this->set('alerts', array_merge($alerts, [
            [
                'type' => $type,
                'text' => $text,
            ]
        ]));
    }
    public function flashAlerts(){
        $alerts = $this->get('alerts');
        $this->remove('alerts');
        return $alerts;
    }
}