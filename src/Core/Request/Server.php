<?php

namespace Request;

class Server implements Action {

    public function set($name, $value)
    {
        $_SERVER[$name] = $value;
    }

    public function get($name)
    {
        return isset($_SERVER[$name]) ? $_SERVER[$name] : null;
    }
    public function setList($list)
    {
        $_SERVER = array_merge($list, $_SERVER);
    }
    public function remove($name)
    {
        unset($_SERVER[$name]);
    }
}