<?php

namespace Request;

class Get implements Action {
    public function set($name, $value)
    {
        $_GET[$name] = $value;
    }

    public function get($name)
    {
        return isset($_GET[$name]) ? $_GET[$name] : null;
    }
    public function setList($list)
    {
        $_GET = array_merge($list, $_GET);
    }
    public function remove($name)
    {
        unset($_GET[$name]);
    }
}