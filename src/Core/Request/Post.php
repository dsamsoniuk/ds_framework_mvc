<?php

namespace Request;

class Post implements Action {
    public function set($name, $value)
    {
        $_POST[$name] = $value;
    }

    public function get($name)
    {
        return isset($_POST[$name]) ? $_POST[$name] : null;
    }
    public function setList($list)
    {
        $_POST = array_merge($list, $_POST);
    }
    public function remove($name)
    {
        unset($_POST[$name]);
    }
}