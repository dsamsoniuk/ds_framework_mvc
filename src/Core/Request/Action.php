<?php

namespace Request;

interface Action {

    public function set($name, $value);

    public function setList($list);

    public function get($name);

    public function remove($name);
}