<?php

namespace Request;

class Cookie implements Action {
    public function set($name, $value)
    {
        $_COOKIE[$name] = $value;
    }

    public function get($name)
    {
        return isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
    }
    public function setList($list)
    {
        $_COOKIE = array_merge($list, $_COOKIE);
    }
    public function remove($name)
    {
        unset($_COOKIE[$name]);
    }
}