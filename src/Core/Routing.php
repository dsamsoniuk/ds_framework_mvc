<?php

namespace Core;

abstract class Routing {

    public $typeVars  = array(
        "string"    => '[a-zA-Z0-9]+',
        "int"       => '[0-9]+'
    );

    public $tracks  = array(
        array(
            'path'         => '/',
            'name'          => 'base',
            'controller'    => 'MainController',
            'method'        => 'index',
        ),
        array(
            'path'         => '/number',
            'name'          => 'randomNumberTest',
            'controller'    => 'MainController',
            'method'        => 'getRandomNumber',
        ),
        array(
            'path'         => '/kontakt/{id}',
            'name'          => 'kontakt',
            'controller'    => 'KontaktController',
            'method'        => 'index',
            'vars'          => array(
                'id'    => 'int',
            )        
        ),
        array(
            'path'         => '/error404',
            'name'          => 'error404',
            'controller'    => 'ErrorController',
            'method'        => 'error404',
        ),
    );
}