<?php

namespace Core;

use Core\Route;
use Controller;
use Request;

class Core {

    private $content;

    public function __construct(){
        try {
            $routing    = new Route();
            $track      = $routing->getControllerByRoute();
            $className  = "\\Controller\\".$track['controller'];

            if (class_exists($className)) {
                $controller = new $className();
            } else {
                throw new \Exception("Brak kontrolera");
            }

            $controller->setDataGET($track['GET']);
            $controller->addGlobalData( $this->getGlobalData($controller) );
            $this->content  = $controller->{$track['method']}();    
        } catch (\Exception $e) {
            // ERROR
            $controller     = new Controller\ErrorController();
            $controller->session->setAlert('danger', $e->getMessage());
            $controller->addGlobalData( $this->getGlobalData($controller) );
            $this->content  = $controller->error404(); 
        }
    }

    public function init(){
        return $this->content;
    }

    public function getGlobalData($controller){
        return [
            'global'    => [
                'widget' =>  new Controller\WidgetController(),
                'session'   => new Request\Session(),
            ],
        ];
    }
}