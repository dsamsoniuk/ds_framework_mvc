<?php

namespace Core;

use LessQL\Database;
use PDO;

class DB {

    private $configPath = '../config.ini';

    public function __construct(){

        try {
            // read parameters in the ini configuration file
            $params = parse_ini_file($this->configPath);
            if ($params === false) {
                throw new \Exception("Error reading database configuration file");
            }
            // connect to the postgresql/sql database
            $conStr     = sprintf("%s:host=%s; dbname=%s",$params['db_type'], $params['host'], $params['database']);
            $pdo        = new PDO($conStr, $params['user'], $params['password']);
            $this->db   = new Database($pdo);
        } catch(\Exception $e) {
            print $e->getMessage();
        }
        
    }
    public function getConnect(){
        return $this->db;
    }
}